# max-flow-julia
Maximum flow solver using [Julia](https://julialang.org/) v0.5.1.

## Implementation
This solver implements the Ford-Fulkerson algorithm using the widest augmenting path. It requires that the input graph follows the [DIMACS](http://dimacs.rutgers.edu/) maximum flow format.

## Usage
`julia maximum-flow.jl < input-graph`


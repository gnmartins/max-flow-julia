include("graph.jl")
include("io.jl")
include("ford-fulkerson.jl")

function main()
    println("[reading dimacs]")
    @time g, src, sink = read_dimacs_max_flow()
    println("[calculating maximum flow]")
    @time maxflow = maximum_flow(g, src, sink)
    println(maxflow)
end

main()

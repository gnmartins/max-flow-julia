type Graph
    edges :: Vector{Dict{Int64, Int64}}
    size :: Int64
    Graph(n :: Int64) = new([Dict{Int64,Int64}() for i = 1 : n], n)
end

# add edge and reversed edge to graph
function add_edge!(g :: Graph, from :: Int64, to :: Int64, capacity :: Int64)
    # normal edge
    g.edges[from][to] = capacity
    # reverse edge
    if !haskey(g.edges[to], from)
        g.edges[to][from] = 0
    end
end

# get capacity from edge
function get_edge(g :: Graph, from :: Int64, to :: Int64)
    if haskey(g.edges[from], to)
        return g.edges[from][to]
    end
    return -1
end

function get_neighbors(g :: Graph, n :: Int64)
    return keys(g.edges[n])
end

# flow amount through path
function flow!(g :: Graph, amount :: Int64, path :: Vector{Int64})
    prev = path[1]
    for i = 2 : size(path, 1)
        current = path[i]
        g.edges[prev][current] -= amount
        g.edges[current][prev] += amount
        prev = current
    end
end

function flow!(g :: Graph, amount :: Int64, previous :: Vector{Int64}, sink :: Int64)
    current = sink
    while current != -1
        prev = previous[current]
        if prev != -1
            g.edges[prev][current] -= amount
            g.edges[current][prev] += amount
        end
        current = prev
    end
end

function read_dimacs_max_flow()

    n, m = 0, 0
    while true
        line = split(readline(STDIN))
        if line[1] == "p"
            if line[2] == "max"
                n, m = parse(Int64, line[3]), parse(Int64, line[4])
                break
            end
        end
    end

    source, sink = 0, 0
    while source == 0 || sink == 0
        line = split(readline(STDIN))
        if line[1] == "n"
            if line[3] == "s"
                source = parse(Int64, line[2])
            elseif line[3] == "t"
                sink = parse(Int64, line[2])
            end
        end
    end

    g = Graph(n)
    edgecount = 0

    while edgecount < m
        line = split(readline(STDIN))
        if line[1] == "a"
            add_edge!(g, 
                      parse(Int64, line[2]),
                      parse(Int64, line[3]), 
                      parse(Int64, line[4]))
            edgecount += 1
        end
    end

    return g, source, sink
end
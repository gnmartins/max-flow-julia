type Node
    e :: Int64
    k :: Int64
    pos :: Int64
    Node(e, k) = new(e, k, -1)
end


type Heap
    nodes :: Array{Node}
    inserts :: Int64
    swaps :: Int64
    updates :: Int64
    deletes :: Int64
    Heap() = new([], 0, 0, 0, 0)
end

root(p :: Int64) = p == 1
parent(p :: Int64) = convert(Int64, floor(p/2))
key(h :: Heap, p :: Int64) = h.nodes[p].k
left(p :: Int64) = 2p
right(p :: Int64) = 2p + 1
getmin(h :: Heap) = h.nodes[1]

function number_of_children(h :: Heap, p :: Int64)
    right(p) <= size(h.nodes, 1) && return 2
    left(p) <= size(h.nodes, 1) && return 1
    return 0
end

function swap!(h :: Heap, pos1 :: Int64, pos2 :: Int64)
    h.swaps += 1
    n1 = h.nodes[pos1]
    n2 = h.nodes[pos2]
    h.nodes[pos2] = n1
    h.nodes[pos1] = n2
    n1.pos = pos2
    n2.pos = pos1
    return h
end

function insert!(h :: Heap, n :: Node, k :: Int64)
    h.inserts += 1 
    n.k = k
    push!(h.nodes, n)
    n.pos = size(h.nodes, 1)
    h = heapify_up!(h, n.pos)
    return h
end

function heapify_up!(h :: Heap, p :: Int64)
    if p == 1 
        return h
    end

    if key(h, parent(p)) < key(h, p)
        h = swap!(h, parent(p), p)
        h = heapify_up!(h, parent(p))
    end
    return h
end

function delete!(h :: Heap, p :: Int64)
    h.deletes += 1
    lastPos = size(h.nodes, 1)
    h = swap!(h, lastPos, p)
    deleted = pop!(h.nodes)
    deleted.pos = -1
    return heapify_down!(h, p)
end

function delete_min!(h :: Heap)
    return delete!(h, 1)
end

function heapify_down!(h :: Heap, p :: Int64)
    if number_of_children(h, p) == 0
        h = h
    end
    
    if number_of_children(h, p) == 1
        if key(h, left(p)) > key(h, p)
            h = swap!(h, left(p), p)
        end
    end

    if number_of_children(h, p) == 2
        if key(h, left(p)) > key(h, p) || key(h, right(p)) > key(h, p)
            if key(h, left(p)) > key(h, right(p))
                h = swap!(h, left(p), p)
                h = heapify_down!(h, left(p))
            else
                h = swap!(h, right(p), p)
                h = heapify_down!(h, right(p))
            end
        end
    end
    return h
end

function increase_key!(h :: Heap, n :: Node, k :: Int64)
    h.updates += 1
    if k > n.k
        n.k = k
        h = heapify_up!(h, n.pos)
    end
    return h
end

include("heap.jl")

function widest_path(g :: Graph, src :: Int64, tgt :: Int64, previous :: Vector{Int64})
    
    fill!(previous, -1)
    
    width = fill(typemin(Int64), g.size)
    width[src] = typemax(Int64)
    
    Q = Heap()
    
    # heap node control
    nodesList = []
    for i = 1:g.size
        push!(nodesList, Node(i, -1))
        Q = insert!(Q, nodesList[i], width[i])
    end
    
    while size(Q.nodes, 1) != 0
        # getting node with max width
        uNode = getmin(Q)
        u = uNode.e
        Q = delete_min!(Q)
        
        width[u] == typemin(Int64) && break
        
        # for all (u, v) edge with capacity w in graph
        for (v, w) in g.edges[u]
            alt = max(width[v], min(width[u], w))
            if alt > width[v]
                width[v] = alt
                previous[v] = u
                Q = increase_key!(Q, nodesList[v], width[v])
            end
        end
    end
   
    return width[tgt]
end
        
            
        




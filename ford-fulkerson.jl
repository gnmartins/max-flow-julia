include("widest-path.jl")

function maximum_flow(g :: Graph, src :: Int64, tgt :: Int64)
    maxflow = 0
    previous = fill(-1, g.size)
    while true
        amount = widest_path(g, src, tgt, previous)
        flow!(g, amount, previous, tgt)
        maxflow += amount
        amount != 0 || break
    end
    return maxflow
end
